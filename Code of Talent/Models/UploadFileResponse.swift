//
//  UploadFileResponse.swift
//  Code of Talent
//
//  Created by Serhii Kobzin on 6/26/18.
//  Copyright © 2018 Onix-Systems. All rights reserved.
//

class UploadFileResponse: Decodable {
    
    var success: Bool
    var trainingRecordFile: TrainingRecordFile
    
    init(success: Bool, trainingRecordFile: TrainingRecordFile) {
        self.success = success
        self.trainingRecordFile = trainingRecordFile
    }
    
}

class TrainingRecordFile: Decodable, Encodable {
    
    var id: Int
    var missionDocument: String
    var ext: String
    var converted: Bool
    var trainingRecord: Int?
    var size: Int
    
    init(id: Int, missionDocument: String, ext: String, converted: Bool, trainingRecord: Int?, size: Int) {
        self.id = id
        self.missionDocument = missionDocument
        self.ext = ext
        self.converted = converted
        self.trainingRecord = trainingRecord
        self.size = size
    }
    
}
