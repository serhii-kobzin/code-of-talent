//
//  MediaParameters.swift
//  Code of Talent
//
//  Created by Serhii Kobzin on 6/19/18.
//  Copyright © 2018 Onix-Systems. All rights reserved.
//

class MediaParameters: Decodable {
    
    var accept: String
    var length: String?
    var quality: String
    var size: String?
    var baseUrl: String?
    
    init(accept: String, length: String?, quality: String, size: String?, baseURL: String?) {
        self.accept = accept
        self.length = length
        self.quality = quality
        self.size = size
        self.baseUrl = baseURL
    }
    
}
