//
//  SideMenuItem.swift
//  Code of Talent
//
//  Created by Serhii Kobzin on 6/13/18.
//  Copyright © 2018 Onix-Systems. All rights reserved.
//

class SideMenuItem: Decodable {
    
    var icon: String
    var label: String
    var type: String
    var action: String?
    var subItems: [SideMenuItem]?
    var notification: Bool?
    var isExpanded: Bool?
    
    init(icon: String, label: String, type: String, action: String?, subItems: [SideMenuItem]?, notification: Bool?, isExpanded: Bool?) {
        self.icon = icon
        self.label = label
        self.type = type
        self.action = action
        self.subItems = subItems
        self.notification = notification
        self.isExpanded = isExpanded
    }
    
}
