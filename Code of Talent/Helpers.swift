//
//  Helpers.swift
//  Code of Talent
//
//  Created by Serhii Kobzin on 6/15/18.
//  Copyright © 2018 Onix-Systems. All rights reserved.
//

import Alamofire
import Foundation

class Helpers {
    
    static func getUnicodeString(hexString: String) -> String {
        guard let code = Int(hexString, radix: 16), let unicodeScalar = UnicodeScalar(code) else {
            return ""
        }
        return String(Character(unicodeScalar))
    }
    
    static func uploadFile(url: URL, remoteURL: URL, completionHandler: @escaping (Data?) -> ()) {
        let filename = url.lastPathComponent
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(url, withName: "data", fileName: filename, mimeType: "video/mp4")
        }, to: remoteURL, encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    print(response)
                    guard let httpResponse = response.response, httpResponse.statusCode == 200 else {
                        completionHandler(nil)
                        return
                    }
                    completionHandler(response.data)
                }
            case .failure(let encodingError):
                print(encodingError.localizedDescription)
                completionHandler(nil)
            }
        })
    }
    
    static func downloadFile(remoteURL: URL, completionHandler: @escaping (Bool) -> ()) {
        let destination = DownloadRequest.suggestedDownloadDestination(for: .documentDirectory)
        let cookies = Helpers.restoreCookies()
        guard let cookieRequestHeader = HTTPCookie.requestHeaderFields(with: cookies).first else {
            completionHandler(false)
            return
        }
        let headers: HTTPHeaders = [cookieRequestHeader.key : cookieRequestHeader.value]
        Alamofire.download(remoteURL, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers, to: destination).responseData(completionHandler: { response in
            print(response)
            guard let httpResponse = response.response, httpResponse.statusCode == 200 else {
                completionHandler(false)
                return
            }
            completionHandler(true)
        })
    }
    
    static func saveCookies(cookies: [HTTPCookie]) {
        var cookiesDictionary = [String : Any]()
        for cookie in cookies {
            cookiesDictionary[cookie.name] = cookie.properties
        }
        UserDefaults.standard.set(cookiesDictionary, forKey: "Cookies")
    }
    
    static func restoreCookies() -> [HTTPCookie] {
        var cookies = [HTTPCookie]()
        guard let cookiesDictionary = UserDefaults.standard.dictionary(forKey: "Cookies") else {
            return cookies
        }
        for cookie in cookiesDictionary {
            if let cookieProperties = cookie.value as? [HTTPCookiePropertyKey : Any], let cookie = HTTPCookie(properties: cookieProperties) {
                cookies.append(cookie)
            }
        }
        return cookies
    }
    
    static func showAlert(title: String, text: String, actionTitle: String?, actionHandler: ((UIAlertAction) -> ())?, showCancel: Bool, presenter: UIViewController?) {
        let alertController = UIAlertController(title: title, message: text, preferredStyle: .alert)
        if let actionTitle = actionTitle, let actionHandler = actionHandler {
            alertController.addAction(UIAlertAction(title: actionTitle, style: .default, handler: actionHandler))
        }
        if showCancel {
            alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        }
        DispatchQueue.main.async {
            presenter?.present(alertController, animated: true)
        }
    }
    
}
