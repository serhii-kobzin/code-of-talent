//
//  SideMenuSubitemCell.swift
//  Code of Talent
//
//  Created by Serhii Kobzin on 6/18/18.
//  Copyright © 2018 Onix-Systems. All rights reserved.
//

import UIKit

class SideMenuSubitemCell: UITableViewCell {
    
    @IBOutlet weak var iconLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    func setup(item: SideMenuItem) {
        iconLabel.text = Helpers.getUnicodeString(hexString: item.icon)
        titleLabel.text = item.label
    }
    
}
