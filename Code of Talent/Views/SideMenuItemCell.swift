//
//  SideMenuItemCell.swift
//  Code of Talent
//
//  Created by Serhii Kobzin on 6/18/18.
//  Copyright © 2018 Onix-Systems. All rights reserved.
//

import UIKit

class SideMenuItemCell: UITableViewCell {
    
    @IBOutlet weak var iconLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var badgeLabel: UILabel!
    @IBOutlet weak var expandImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        badgeLabel.text = "\u{eae9}"
    }
    
    func setup(item: SideMenuItem) {
        iconLabel.text = Helpers.getUnicodeString(hexString: item.icon)
        titleLabel.text = item.label
        expandImageView.isHidden = item.type.lowercased() != "group"
        expandImageView.transform = CGAffineTransform(scaleX: 1, y: item.isExpanded == true ? -1 : 1)
        badgeLabel.isHidden = !(item.notification ?? false)
    }
    
}
