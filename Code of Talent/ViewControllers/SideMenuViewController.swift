//
//  SideMenuViewController.swift
//  Code of Talent
//
//  Created by Serhii Kobzin on 6/13/18.
//  Copyright © 2018 Onix-Systems. All rights reserved.
//

import UIKit

class SideMenuViewController: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var sideMenuView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var contentView: UIView!
    
    var sideMenuItems = [SideMenuItem]()
    var contentViewController: ContentViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.setNeedsLayout()
        view.layoutIfNeeded()
        addShadows()
        setupTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        hideSideMenu(animated: false)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        let contentOffset = scrollView.contentOffset
        coordinator.animate(alongsideTransition: { [weak self] context in
            self?.scrollView.setContentOffset(contentOffset, animated: false)
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "embedSegue", let navigationController = segue.destination as? UINavigationController, let viewController = navigationController.viewControllers.first as? ContentViewController {
            contentViewController = viewController
            contentViewController?.delegate = self
        }
    }
    
}

extension SideMenuViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sideMenuItems.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let item = sideMenuItems[section]
        if item.isExpanded != true {
            return 0
        }
        return item.subItems?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let item = sideMenuItems[section]
        let cell = tableView.dequeueReusableCell(withIdentifier: "Side Menu Item Cell") as? SideMenuItemCell
        cell?.setup(item: item)
        cell?.tag = section
        let tapGestureRecongizer = UITapGestureRecognizer(target: self, action: #selector(handleTapGesture(_:)))
        cell?.addGestureRecognizer(tapGestureRecongizer)
        return cell
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = indexPath.section
        let row = indexPath.row
        let item = sideMenuItems[section]
        guard let subitems = item.subItems, let cell = tableView.dequeueReusableCell(withIdentifier: "Side Menu Subitem Cell", for: indexPath) as? SideMenuSubitemCell else {
            return UITableViewCell()
        }
        let subitem = subitems[row]
        cell.setup(item: subitem)
        return cell
    }
    
}

extension SideMenuViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let section = indexPath.section
        let row = indexPath.row
        let item = sideMenuItems[section]
        guard let subitems = item.subItems else {
            return
        }
        let subitem = subitems[row]
        executeAction(of: subitem)
    }
    
}

extension SideMenuViewController: ContentViewControllerDelegate {
    
    func switchSideMenu(animated: Bool) {
        if scrollView.contentOffset == CGPoint.zero {
            hideSideMenu(animated: animated)
        } else {
            showSideMenu(animated: animated)
        }
    }
    
    func showSideMenu(animated: Bool) {
        if scrollView.contentOffset == CGPoint.zero {
            return
        }
        scrollView.setContentOffset(CGPoint.zero, animated: animated)
    }
    
    func hideSideMenu(animated: Bool) {
        if scrollView.contentOffset == contentView.frame.origin {
            return
        }
        scrollView.setContentOffset(contentView.frame.origin, animated: animated)
    }
    
    func setupSideMenu(items: [SideMenuItem]) {
        sideMenuItems = items
        tableView.reloadData()
    }
    
}

extension SideMenuViewController {
    
    func addShadows() {
        contentView.layer.shadowColor = UIColor.black.cgColor
        contentView.layer.shadowOpacity = 1
        contentView.layer.shadowOffset = CGSize.zero
        contentView.layer.shadowRadius = 3
    }
    
    func setupTableView() {
        tableView.tableFooterView = UIView()
    }
    
    @objc func handleTapGesture(_ sender: UITapGestureRecognizer) {
        guard let section = sender.view?.tag else {
            return
        }
        let item = sideMenuItems[section]
        executeAction(of: item)
    }
    
    func executeAction(of item: SideMenuItem) {
        switch item.type.lowercased() {
        case "url":
            hideSideMenu(animated: true)
            if let action = item.action {
                contentViewController?.urlString = action
                contentViewController?.executeRequest()
            }
        case "qrscanner":
            hideSideMenu(animated: true)
            contentViewController?.requestForCameraUse(completionHandler: { [weak self] success in
                if success {
                    self?.contentViewController?.presentQRScanner()
                }
            })
        case "group":
            item.isExpanded = !(item.isExpanded ?? false)
            tableView.reloadData()
        case "js":
            hideSideMenu(animated: true)
            if let action = item.action {
                contentViewController?.executeJavaScript(code: action)
            }
        default:
            hideSideMenu(animated: true)
        }
    }
    
}
