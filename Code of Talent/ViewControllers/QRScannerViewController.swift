//
//  QRScannerViewController.swift
//  Code of Talent
//
//  Created by Serhii Kobzin on 10/18/18.
//  Copyright © 2018 Onix-Systems. All rights reserved.
//

import AVFoundation
import UIKit

protocol QRScannerDelegate {
    
    func qrCodeDidRecognized(urlString: String)
    
}

class QRScannerViewController: UIViewController {
    
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var scanView: UIView!
    
    var captureSession: AVCaptureSession = AVCaptureSession()
    var videoPreviewLayer: AVCaptureVideoPreviewLayer = AVCaptureVideoPreviewLayer()
    var delegate: QRScannerDelegate?
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        videoView.setNeedsLayout()
        videoView.layoutIfNeeded()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        addVideoLayer()
    }
    
    @IBAction func cancelButtonTouchUpInside(_ sender: UIButton) {
        dismiss(animated: true)
    }
    
}

extension QRScannerViewController: AVCaptureMetadataOutputObjectsDelegate {
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        guard let code = metadataObjects.first as? AVMetadataMachineReadableCodeObject, code.type == .qr, let codeRect = videoPreviewLayer.transformedMetadataObject(for: code)?.bounds, scanView.frame.contains(codeRect), let value = code.stringValue, let _ = URL(string: value) else {
            return
        }
        guard value.contains("codeoftalent.com") else {
            captureSession.stopRunning()
            Helpers.showAlert(title: "Error", text: "This QR Code is not supported", actionTitle: "OK", actionHandler: { [weak self] _ in
                self?.captureSession.startRunning()
            }, showCancel: false, presenter: self)
            return
        }
        dismiss(animated: true, completion: { [weak self] in
            self?.delegate?.qrCodeDidRecognized(urlString: value)
        })
    }
    
}

extension QRScannerViewController {
    
    func addVideoLayer() {
        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else {
            return
        }
        let videoInput: AVCaptureDeviceInput
        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            print(error.localizedDescription)
            return
        }
        if captureSession.canAddInput(videoInput) {
            captureSession.addInput(videoInput)
        } else {
            return
        }
        let metadataOutput = AVCaptureMetadataOutput()
        if captureSession.canAddOutput(metadataOutput) {
            captureSession.addOutput(metadataOutput)
        } else {
            return
        }
        metadataOutput.metadataObjectTypes = [.qr]
        metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        videoPreviewLayer.videoGravity = .resizeAspectFill
        videoPreviewLayer.frame = videoView.bounds
        videoView.layer.addSublayer(videoPreviewLayer)
        captureSession.startRunning()
//        metadataOutput.rectOfInterest = videoPreviewLayer.metadataOutputRectConverted(fromLayerRect: scanView.frame)
    }
    
}
