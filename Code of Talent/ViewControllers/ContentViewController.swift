//
//  ContentViewController.swift
//  Code of Talent
//
//  Created by Serhii Kobzin on 6/13/18.
//  Copyright © 2018 Onix-Systems. All rights reserved.
//

import AVKit
import MobileCoreServices
import Photos
import UIKit
import WebKit

protocol ContentViewControllerDelegate: class {
    
    func switchSideMenu(animated: Bool)
    func setupSideMenu(items: [SideMenuItem])
    
}

class ContentViewController: UIViewController {
    
    @IBOutlet weak var leftBarButtonItem: UIBarButtonItem!
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var darkView: UIView!
    
    weak var delegate: ContentViewControllerDelegate?
    var mediaParameters: MediaParameters?
    var needShowActivityIndicator = true
    
    var urlString = loginURLString
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        setupWebView()
        executeRequest()
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "estimatedProgress" {
            if webView.estimatedProgress == 1.0 {
                hideActivityIndicator()
                needShowActivityIndicator = true
                urlString = webView.url?.absoluteString ?? urlString
                webView.configuration.websiteDataStore.httpCookieStore.getAllCookies({ cookies in
                    Helpers.saveCookies(cookies: cookies)
                })
            } else {
                if needShowActivityIndicator {
                    showActivityIndicator()
                }
            }
        }
    }
    
    // MARK: - IBActions
    @IBAction func leftBarButtonItemTouchUpInside(_ sender: UIBarButtonItem) {
        switch sender.tag {
        case 0:
            delegate?.switchSideMenu(animated: true)
        case 1:
            webView.goBack()
        default:
            break
        }
    }
    
}

// MARK: - WKNavigationDelegate
extension ContentViewController: WKNavigationDelegate {
    
//    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
//        if needShowActivityIndicator {
//            showActivityIndicator()
//        }
//        decisionHandler(.allow)
//        var request = navigationAction.request
//        var allRequestHeaders = request.allHTTPHeaderFields ?? [ : ]
//        let keys = allRequestHeaders.keys
//        if Array(keys).contains("Cookie") {
//            decisionHandler(.allow)
//            return
//        }
//        webView.configuration.websiteDataStore.httpCookieStore.getAllCookies({ cookies in
//            guard let cookieRequestHeader = HTTPCookie.requestHeaderFields(with: cookies).first else {
//                decisionHandler(.allow)
//                return
//            }
//            decisionHandler(.cancel)
//            allRequestHeaders[cookieRequestHeader.key] = cookieRequestHeader.value
//            request.allHTTPHeaderFields = allRequestHeaders
//            webView.load(request)
//        })
//    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
        if let url = webView.url, let mimeType = navigationResponse.response.mimeType, mimeType != "text/html" {
            webView.stopLoading()
            downloadFile(remoteURL: url)
        }
        decisionHandler(.allow)
    }
    
//    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
//        hideActivityIndicator()
//        needShowActivityIndicator = true
//        urlString = webView.url?.absoluteString ?? urlString
//    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        print("didFail: \(error.localizedDescription)")
    }
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print("didFailProvisionalNavigation: \(error.localizedDescription)")
    }
    
}

// MARK: - WKScriptMessageHandler
extension ContentViewController: WKScriptMessageHandler {
    
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        switch message.name {
        case "sendMenu":
            parseMenu(messageBody: message.body)
        case "videoCapture":
            parseMediaParameters(messageBody: message.body)
        case "photoCapture":
            parseMediaParameters(messageBody: message.body)
        case "getDeviceID":
            sendDeviceIdToJS()
        default:
            break
        }
    }
    
}

// MARK: - UIImagePickerControllerDelegate
extension ContentViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage, let url = saveImageToDocumentsDirectory(image: image) {
            dismiss(animated: true, completion: { [weak self] in
                self?.showActivityIndicator()
                self?.uploadFile(url: url)
                self?.hideActivityIndicator()
            })
            return
        }
        if let url = info[UIImagePickerControllerMediaURL] as? URL {
            if url.pathExtension.lowercased().contains("mov") {
                dismiss(animated: true, completion: { [weak self] in
                    self?.showActivityIndicator()
                    self?.convertVideo(url: url, completionHandler: { url in
                        guard let url = url else {
                            self?.hideActivityIndicator()
                            return
                        }
                        self?.uploadFile(url: url)
                    })
                })
                return
            }
            dismiss(animated: true, completion: { [weak self] in
                self?.showActivityIndicator()
                self?.uploadFile(url: url)
                self?.hideActivityIndicator()
            })
            return
        }
        dismiss(animated: true)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true)
    }
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        let viewControllerClass = String(describing: viewController.self)
        guard viewControllerClass.contains("PUPhotoPickerHostViewController") else {
            return
        }
        viewController.navigationItem.title = mediaParameters?.accept == "video/*" ? "Videos" : "Photos"
    }
    
}

extension ContentViewController: QRScannerDelegate {
    
    func qrCodeDidRecognized(urlString: String) {
        self.urlString = urlString
        executeRequest()
    }
    
}

// MARK: - Custom Functions
extension ContentViewController {
    
    func setupNavigationBar() {
        navigationItem.titleView = UIImageView(image: #imageLiteral(resourceName: "TitleLogo"))
    }
    
    func setupLeftBarButtonItem(tag: Int) {
        leftBarButtonItem.tag = tag
        leftBarButtonItem.image = tag == 0 ? #imageLiteral(resourceName: "MenuIcon") : #imageLiteral(resourceName: "BackIcon")
    }
    
    func setupWebView() {
        webView.navigationDelegate = self
        let scrollView = webView.scrollView
        scrollView.bouncesZoom = false
        scrollView.isDirectionalLockEnabled = true
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshWebView(sender:)), for: .valueChanged)
        webView.scrollView.addSubview(refreshControl)
        let metaScriptCode = "var meta = document.createElement('meta');" +
                             "meta.name = 'viewport';" +
                             "meta.content = 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no';" +
                             "var head = document.getElementsByTagName('head')[0];" +
                             "head.appendChild(meta);"
        let metaScript = WKUserScript(source: metaScriptCode, injectionTime: .atDocumentEnd, forMainFrameOnly: true)
        let contentController = webView.configuration.userContentController
        contentController.addUserScript(metaScript)
        contentController.add(self, name: "sendMenu")
        contentController.add(self, name: "videoCapture")
        contentController.add(self, name: "photoCapture")
        contentController.add(self, name: "getDeviceID")
        let cookies = Helpers.restoreCookies()
        for cookie in cookies {
            webView.configuration.websiteDataStore.httpCookieStore.setCookie(cookie)
        }
        webView.addObserver(self, forKeyPath: #keyPath(WKWebView.estimatedProgress), options: .new, context: nil)
    }
    
    @objc func refreshWebView(sender: UIRefreshControl) {
        needShowActivityIndicator = false
        executeRequest()
        sender.endRefreshing()
    }
    
    func showActivityIndicator() {
        DispatchQueue.main.async { [weak self] in
            if let `self` = self, !self.darkView.isHidden {
                return
            }
            self?.darkView.isHidden = false
        }
    }
    
    func hideActivityIndicator() {
        DispatchQueue.main.async { [weak self] in
            if let `self` = self, self.darkView.isHidden {
                return
            }
            self?.darkView.isHidden = true
        }
    }
    
    func executeRequest() {
        guard let url = URL(string: self.urlString) else {
            return
        }
        let request = URLRequest(url: url)
        self.webView.load(request)
    }
    
    func parseMenu(messageBody: Any) {
        guard let message = messageBody as? String, let data = message.data(using: .utf8), let items = try? JSONDecoder().decode([SideMenuItem].self, from: data) else {
            setupLeftBarButtonItem(tag: 1)
            return
        }
        setupLeftBarButtonItem(tag: 0)
        delegate?.setupSideMenu(items: items)
    }
    
    func parseMediaParameters(messageBody: Any) {
        guard let message = messageBody as? [String : String], let data = try? JSONSerialization.data(withJSONObject: message, options: []), let parameters = try? JSONDecoder().decode(MediaParameters.self, from: data) else {
            return
        }
        mediaParameters = parameters
        showActionSheet()
    }
    
    func parseUploadFileResponse(response: Data) {
        guard let response = try? JSONDecoder().decode(UploadFileResponse.self, from: response) else {
            return
        }
        sendResponseToJS(response: response.trainingRecordFile)
    }
    
    func showActionSheet() {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alertController.addAction(UIAlertAction(title: mediaParameters?.accept == "video/*" ? "Take a video" : "Take a photo", style: .default, handler: { [weak self] _ in
            self?.requestForCameraUse(completionHandler: { [weak self] success in
                if success {
                    self?.presentCamera()
                }
            })
        }))
        alertController.addAction(UIAlertAction(title: "Select from gallery", style: .default, handler: { [weak self] _ in
            self?.requestForGalleryUse()
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        DispatchQueue.main.async { [weak self] in
            self?.present(alertController, animated: true)
        }
    }
    
    func requestForCameraUse(completionHandler: @escaping (Bool) -> ()) {
        if !UIImagePickerController.isSourceTypeAvailable(.camera) {
            print("Camera is not available on this device")
            completionHandler(false)
            return
        }
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .authorized:
            completionHandler(true)
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { granted in
                if !granted {
                    print("Access to camera is not granted")
                    completionHandler(false)
                    return
                }
                completionHandler(true)
            })
        default:
            completionHandler(false)
        }
    }
    
    func requestForGalleryUse() {
        switch PHPhotoLibrary.authorizationStatus() {
        case .authorized:
            DispatchQueue.main.async { [weak self] in
                self?.presentGallery()
            }
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization({ [weak self] status in
                if status != .authorized {
                    print("Access to gallery is not granted")
                    return
                }
                self?.presentGallery()
            })
        case .denied:
            print("Access to gallery is not granted")
        default:
            break
        }
    }
    
    func presentCamera() {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.sourceType = .camera
        if mediaParameters?.accept == "video/*" {
            imagePickerController.mediaTypes = [kUTTypeMovie as String]
            imagePickerController.cameraCaptureMode = .video
            let quality = mediaParameters?.quality ?? "640"
            switch quality {
            case "320":
                imagePickerController.videoQuality = .typeMedium
            case "640":
                imagePickerController.videoQuality = .type640x480
            case "720":
                imagePickerController.videoQuality = .typeIFrame960x540
            case "1280":
                imagePickerController.videoQuality = .typeIFrame1280x720
            case "1920":
                imagePickerController.videoQuality = .typeHigh
            default:
                break
            }
            imagePickerController.videoMaximumDuration = Double((Int(mediaParameters?.length ?? "120000") ?? 120000) / 1000)
        }
        if mediaParameters?.accept == "image/*" {
            imagePickerController.cameraCaptureMode = .photo
        }
        DispatchQueue.main.async { [weak self] in
            self?.present(imagePickerController, animated: true)
        }
    }
    
    func presentQRScanner() {
        guard let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "QR Scanner View Controller") as? QRScannerViewController else {
            return
        }
        viewController.delegate = self
        DispatchQueue.main.async { [weak self] in
            self?.present(viewController, animated: true)
        }
    }
    
    func presentGallery() {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.sourceType = .photoLibrary
        if mediaParameters?.accept == "video/*" {
            imagePickerController.mediaTypes = [kUTTypeMovie as String]
        }
        DispatchQueue.main.async { [weak self] in
            self?.present(imagePickerController, animated: true)
        }
    }
    
    func convertVideo(url: URL, completionHandler: @escaping (URL?) -> ()) {
        let outputURL = url.deletingPathExtension().appendingPathExtension("mp4")
        let asset = AVURLAsset(url: url)
        let quality = mediaParameters?.quality ?? "640"
        var presetName = AVAssetExportPreset640x480
        switch quality {
        case "320":
            presetName = AVAssetExportPresetMediumQuality
        case "640":
            presetName = AVAssetExportPreset640x480
        case "720":
            presetName = AVAssetExportPreset960x540
        case "1280":
            presetName = AVAssetExportPreset1280x720
        case "1920":
            presetName = AVAssetExportPreset1920x1080
        default:
            break
        }
        guard let exportSession = AVAssetExportSession(asset: asset, presetName: presetName) else {
            completionHandler(nil)
            return
        }
        exportSession.outputURL = outputURL
        exportSession.outputFileType = AVFileType.mp4
        let start = CMTimeMakeWithSeconds(0.0, 0)
        let range = CMTimeRangeMake(start, asset.duration)
        exportSession.timeRange = range
        exportSession.shouldOptimizeForNetworkUse = true
        exportSession.exportAsynchronously() {
            switch exportSession.status {
            case .failed:
                print("Export failed: \(exportSession.error != nil ? exportSession.error!.localizedDescription : "No Error Info")")
            case .cancelled:
                print("Export canceled")
            case .completed:
                completionHandler(outputURL)
            default:
                break
            }
        }
    }
    
    func saveImageToDocumentsDirectory(image: UIImage) -> URL? {
        guard let data = UIImageJPEGRepresentation(image, 1.0), let documentDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else {
            return nil
        }
        let filename = UUID().uuidString + ".jpg"
        let url = documentDirectory.appendingPathComponent(filename)
        do {
            try data.write(to: url)
            return url
        }
        catch let error {
            print(error.localizedDescription)
            return nil
        }
    }
    
    func saveFileToDocumentsDirectory(data: Data) -> URL? {
        guard let documentDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else {
            return nil
        }
        let filename = UUID().uuidString + ".jpg"
        let url = documentDirectory.appendingPathComponent(filename)
        do {
            try data.write(to: url)
            return url
        }
        catch let error {
            print(error.localizedDescription)
            return nil
        }
    }
    
    func uploadFile(url: URL) {
        guard let urlString = mediaParameters?.baseUrl, let remoteURL = URL(string: urlString) else {
            hideActivityIndicator()
            return
        }
        showActivityIndicator()
        Helpers.uploadFile(url: url, remoteURL: remoteURL, completionHandler: { [weak self] response in
            self?.hideActivityIndicator()
            guard let response = response else {
                Helpers.showAlert(title: "Error", text: "An error occurred while uploading the file", actionTitle: "Try again", actionHandler: { _ in
                    self?.uploadFile(url: url)
                }, showCancel: true, presenter: self)
                return
            }
            self?.parseUploadFileResponse(response: response)
        })
    }
    
    func downloadFile(remoteURL: URL) {
        Helpers.downloadFile(remoteURL: remoteURL, completionHandler: { [weak self] success in
            self?.hideActivityIndicator()
            if !success {
                Helpers.showAlert(title: "Error", text: "An error occurred while downloading the file", actionTitle: nil, actionHandler: nil, showCancel: true, presenter: self)
                return
            }
        })
    }
    
    func sendDeviceIdToJS() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let deviceToken = appDelegate.deviceToken ?? ""
        let sendDeviceIdScriptCode = "saveDeviceID('\(deviceToken)')"
        webView.evaluateJavaScript(sendDeviceIdScriptCode)
    }
    
    func sendResponseToJS(response: TrainingRecordFile) {
        guard let data = try? JSONEncoder().encode(response), let responseJSON = String(data: data, encoding: .utf8) else {
            return
        }
        let fileUploadScriptCode = "fileUploaded('\(responseJSON)')"
        executeJavaScript(code: fileUploadScriptCode)
    }
    
    func executeJavaScript(code: String) {
        webView.evaluateJavaScript(code)
    }
    
}
